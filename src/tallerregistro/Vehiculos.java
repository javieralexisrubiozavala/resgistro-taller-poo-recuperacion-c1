/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerregistro;

/**
 *
 * @author 52667
 */
public abstract class Vehiculos {
    protected int vehiculoSerie;
    protected int tipoMotor;
    protected String marca;
    protected String modelo;

    public Vehiculos(int vehiculoSerie, int tipoMotor, String marca, String modelo) {
        this.vehiculoSerie = vehiculoSerie;
        this.tipoMotor = tipoMotor;
        this.marca = marca;
        this.modelo = modelo;
    }

    public Vehiculos() {
        this.vehiculoSerie = 0;
        this.tipoMotor = 0;
        this.marca = "";
        this.modelo = "";
        
    }
  
    
    
    
    public int getVehiculoSerie() {
        return vehiculoSerie;
    }

    public void setVehiculoSerie(int vehiculoSerie) {
        this.vehiculoSerie = vehiculoSerie;
    }

    public int getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(int tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    


    
  
     
    
    
}
