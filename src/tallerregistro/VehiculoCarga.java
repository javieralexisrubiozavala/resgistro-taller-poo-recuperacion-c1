/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerregistro;

/**
 *
 * @author 52667
 */
public class VehiculoCarga extends Vehiculos {
    private float toneladaCarga ;

    public VehiculoCarga(float toneladaCarga, int vehiculoSerie, int tipoMotor, String marca, String modelo) {
        super(vehiculoSerie, tipoMotor, marca, modelo);
        this.toneladaCarga = toneladaCarga;
    }



    public VehiculoCarga() {
        this.toneladaCarga = 0.0f;
        
    }

    public float getToneladaCarga() {
        return toneladaCarga;
    }

    public void setToneladaCarga(float toneladaCarga) {
        this.toneladaCarga = toneladaCarga;
    }
    
    
    
    
}
