package tallerregistro;

        
public class RegistroServicio {
    private int numServicio;
    private String fecha;
    private int tipoServicio; // 1 para reparacion 2 para mantenimiento
    private VehiculoCarga vehiculoCarga;
    private String descripcionAuto;
    private float costoBaseMantenimiento;

    public RegistroServicio(int numServicio, String fecha, int tipoServicio, VehiculoCarga vehiculoCarga, String descripcionAuto, float costoBaseMantenimiento, int vehiculoSerie, int tipoMotor, String marca, String modelo) {
 
        this.numServicio = numServicio;
        this.fecha = fecha;
        this.tipoServicio = tipoServicio;
        this.vehiculoCarga = vehiculoCarga;
        this.descripcionAuto = descripcionAuto;
        this.costoBaseMantenimiento = costoBaseMantenimiento;
    }

    public RegistroServicio() {
        super();
        this.numServicio = 0;
        this.fecha = "";
        this.vehiculoCarga = new VehiculoCarga();
        this.tipoServicio = 0;
        this.descripcionAuto = "";
        this.costoBaseMantenimiento = 0;
    }
    
    public int getNumServicio() {
        return numServicio;
    }

    public void setNumServicio(int numServicio) {
        this.numServicio = numServicio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public VehiculoCarga getVehiculoCarga() {
        return vehiculoCarga;
    }

    public void setVehiculoCarga(VehiculoCarga vehiculoCarga) {
        this.vehiculoCarga = vehiculoCarga;
    }

    public String getDescripcionAuto() {
        return descripcionAuto;
    }

    public void setDescripcionAuto(String descripcionAuto) {
        this.descripcionAuto = descripcionAuto;
    }

    public float getCostoBaseMantenimiento() {
        return costoBaseMantenimiento;
    }

    public void setCostoBaseMantenimiento(float costoBaseMantenimiento) {
        this.costoBaseMantenimiento = costoBaseMantenimiento;
    }

   
          public float costoServicio(int tipoMotor) {
        float incremento = 0.0f;
        switch (tipoMotor) {
            case 0:
                incremento = this.costoBaseMantenimiento * 0.25f;
                break;
            case 1:
                incremento = this.costoBaseMantenimiento * 0.50f;
                break;
            case 2:
                incremento = this.costoBaseMantenimiento * 1.50f;
                break;
            case 3:
                incremento = this.costoBaseMantenimiento * 2.0f;
                break;
            default:
                System.out.println("Tipo de motor no válido");
                return this.costoBaseMantenimiento;
        }
        return this.costoBaseMantenimiento + incremento;
    }
    
   
   public float costoImpuesto(int tipoMotor) {
        float costoServicio = this.costoServicio(tipoMotor);
        return costoServicio * 0.16f;
    }

    
    public float calcularTotal(int tipoMotor) {
        float costoServicio = this.costoServicio(tipoMotor);
        float costoImpuesto = this.costoImpuesto(tipoMotor);
    return costoServicio + costoImpuesto;
}
    
    
    
    
    
}
    
    


